dependencies {
    implementation(project(":HoTT-Model"))
    implementation("de.treichels.hott:hott-decoder:_")
    implementation(project(":HoTT-Voice"))
    implementation("org.freemarker:freemarker:_")
    implementation("commons-io:commons-io:_")
    implementation("no.tornado:tornadofx:_")
}
